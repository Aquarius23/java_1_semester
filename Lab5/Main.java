package lab5;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
  public static void main(String[] args) {
    int number = 6;
    Class<AnnotationClass.ActionsWithNumbers> cl = AnnotationClass.ActionsWithNumbers.class;
    AnnotationClass.ActionsWithNumbers classInstance = new AnnotationClass.ActionsWithNumbers();
    try {
      for (Method i : cl.getDeclaredMethods()) {
        if (i.isAnnotationPresent(numOfCalls.class)) {
          i.setAccessible(true);
          int value = i.getAnnotation(numOfCalls.class).value();
          for (int j = 0; j < value; ++j) {
            i.invoke(classInstance, number);
          }
        }
      }
    } catch (IllegalAccessException | InvocationTargetException error) {
      error.printStackTrace();
    } catch (Exception error) {
      error.printStackTrace();
    }
  }
}
