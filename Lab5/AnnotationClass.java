package lab5;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface numOfCalls {
  int value() default 0;
}
public class AnnotationClass {
  public static class ActionsWithNumbers {
    @numOfCalls(1)
    private void printSquareOfNumber(int number) {
      int result = number * number;
      System.out.print("Квадрат числа " + number + " : " + result + System.lineSeparator());
    }

    @numOfCalls(2)
    private void printRootOfNumber(int number) throws Exception {
      if (number < 0) {
        throw new Exception("Не можем найти корень из отрицательного числа" + System.lineSeparator());
      }
      int result = (int)Math.sqrt(number);
      System.out.print("Корень числа " + number + " : " + result + System.lineSeparator());
    }

    @numOfCalls(3)
    private void printNumberOfDigits(int number) throws Exception {
      if (number < 0) {
        throw new Exception("Не можем найти количество разрядов отрицательного числа" + System.lineSeparator());
      }
      int digit = 0;
      while (number > 0)
      {
        number /= 10;
        digit++;
      }
      System.out.print("Количество разрядов числа " + number + " : " + digit + System.lineSeparator());
    }
  }
}
