package lab2;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;

public class Task5
{
  private static boolean compareString(String lhs, String rhs)
  {
    if (lhs.charAt(0) > rhs.charAt(0))
      return true;
    else if (lhs.charAt(1) > rhs.charAt(1))
      return true;
    else if (lhs.charAt(2) > rhs.charAt(2))
      return true;
    else
      return false;
  }
  private static void insertionSort(String[] array, int size)
  {
    String temp = new String();
    int index = 0;
    for (int i = 0; i < size; ++i)
    {
      temp = array[i];
      index = i;
      while (index > 0 && compareString(array[index - 1], temp))
      {
        array[index] = array[index - 1];
        --index;
      }
      array[index] = temp;
    }
  }
  private static String randomMiddle(String word)
  {
    Random rand = new Random();
    char[] temp = new char[3];
    temp[0] = word.charAt(0);
    temp[2] = word.charAt(2);
    char mid = (char)(rand.nextInt(26) + 'a');
    if (mid == temp[0] || mid == temp[2])
      mid = (char)(rand.nextInt(26) + 'a');
    temp[1] = mid;
    return String.valueOf(temp);
  }
  public static void main(String[] args)
  {
    Scanner stream = new Scanner(System.in);
    StringBuilder str = new StringBuilder(stream.nextLine());
    System.out.print("Исходная строка: ");
    System.out.print(str);
    System.out.print(System.lineSeparator());
    int size = str.length()/3;
    String[] array = new String[size];
    int count = 0, begin = 0, end = 3;
    while (count < size)
    {
      if (end <= str.length())
      {
        char[] temp = new char[3];
        str.getChars(begin, end, temp, 0);
        begin += 3;
        end += 3;
        array[count] = String.valueOf(temp);
        ++count;
      }
      else
        break;
    }
    System.out.print("Строка после разделения: ");
    for (int i = 0; i < size; ++i)
      System.out.print(array[i] + " ");
    System.out.print(System.lineSeparator());
    for (int j = 0; j < size; ++j)
    {
      String temp = new String(randomMiddle(array[j]));
      array[j] = temp;
    }
    insertionSort(array, size);
    System.out.print("Преобразованная строка: ");
    for (int k = 0; k < size; ++k)
      System.out.print(array[k] + ", ");
    System.out.print(System.lineSeparator());
  }
}
