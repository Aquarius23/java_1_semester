package lab2;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;


public class Task2 {
  public static void main(String[] args)
  {
    try
    {
      File file = new File("example.txt");
      FileReader fr = new FileReader(file);
      BufferedReader reader = new BufferedReader(fr);
      String line = reader.readLine();
      TreeMap<String, Integer> tree = new TreeMap<>();
      while (line != null)
      {
        String[] words = line.split(" ");
        if (tree.containsKey(words[1]))
        {
          int count = tree.get(words[1]) + 1;
          tree.put(words[1], count);
        }
        else
          tree.put(words[1], 1);
        line = reader.readLine();
      }
      System.out.print("Топ3 самых популярных пароля:" + System.lineSeparator());
      for (int i = 0; i < 3; ++i)
      {
        int maxCount = 0;
        String maxValue = new String();
        for(Object key:tree.keySet())
        {
          if ((int)tree.get(key) > maxCount)
          {
            maxCount = (int)tree.get(key);
            maxValue = (String)key;
          }
        }
        System.out.print(i + 1 + " : " + maxValue);
        System.out.print(System.lineSeparator());
        tree.remove(maxValue);
      }
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
