package lab2;
import java.util.Scanner;

public class Task4
{
  public static void main(String[] args)
  {
    Scanner stream = new Scanner(System.in);
    StringBuilder str = new StringBuilder(stream.nextLine());
    System.out.print("Исходная строка:");
    System.out.print(str + System.lineSeparator());
    int i = 0, count = 0, begin = 0, end = 0;
    while (str.charAt(i) == ' ')
      ++i;
    count = i;
    end = i;
    if (count > 0)
      str.delete(begin, end);
    count = 0;
    end = 0;
    for (int j = 0; j < str.length(); ++j)
    {
      if (str.charAt(j) == ' ')
      {
        if (count == 0)
          ++count;
        else if (count == 1)
        {
          ++count;
          begin = j;
          end = j;
        }
        else if (count > 1)
        {
          ++count;
          end = j;
        }
      }
      else
      {
        if (count > 1)
        {
          str.delete(begin, end + 1);
          begin = 0;
          end = 0;
          count = 0;
        }
      }
    }
    if (count > 1)
      str.delete(begin, end + 1);
    System.out.print("Исправленная строка:");
    System.out.print(str + System.lineSeparator());
    stream.close();
  }
}
