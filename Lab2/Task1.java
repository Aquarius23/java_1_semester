package lab2;
import java.util.Scanner;

public class Task1 {
  private static final String digit1[][] = {{"один", "два", "три", "четыре", "пять", "шесть",
      "семь", "восемь", "девять"}, {"одна", "две"}};
  private static final String digit10[] = {"десять", "одиннадцать", "двенадцать", "тринадцать",
      "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
  private static final String digit20[] = {"двадцать", "тридцать", "сорок", "пятьдесят",
      "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
  private static final String digit100[] = {"сто", "двести", "триста", "четыреста", "пятьсот",
      "шестьсот", "семьсот", "восемьсот", "девятьсот"};
  private static final String moreDigit[][] = {{"тысяча", "тысячи", "тысяч"}, {"миллион",
      "миллиона", "миллионов"}, {"миллиард", "миллиарда"}};
  private static int getCountsOfDigits(int number) {
    int count = (number == 0) ? 1 : 0;
    while (number != 0) {
      count++;
      number /= 10;
    }
    return count;
  }
  private static int getCountByDegree(int degree)
  {
    int number = 1;
    while (degree > 0)
    {
      number *=10;
      degree--;
    }
    return number;
  }
  private static void printMilliard(int number, int degree)
  {
    int dig1 = number / getCountByDegree(degree);
    if (dig1 == 1)
      System.out.print(digit1[0][dig1 - 1] + " " + moreDigit[2][0] + " ");
    else
      System.out.print(digit1[0][dig1 - 1] + " " + moreDigit[2][1] + " ");
  }
  private static void printDigit100(int dig100)
  {
    System.out.print(digit100[dig100 - 1] + " ");
  }
  private static void printDigit10(int dig10, int dig1)
  {
    if (dig10 > 1)
      System.out.print(digit20[dig10 - 2] + " ");
    else if (dig10 == 1)
      System.out.print(digit10[dig1] + " ");
  }
  private static void printDigit1(int dig1, int nameOfDigit)
  {
    if (dig1 > 2)
      System.out.print(digit1[0][dig1 - 1] + " ");
    else if (dig1 == 1 || dig1 == 2)
    {
      if (nameOfDigit == 2 || nameOfDigit == 0)
        System.out.print(digit1[0][dig1 - 1] + " ");
      else
        System.out.print(digit1[1][dig1 - 1] + " ");
    }
    if (nameOfDigit != 0)
    {
      if (dig1 == 1)
        System.out.print(moreDigit[nameOfDigit - 1][0] + " ");
      else if (dig1 > 1 && dig1 < 5)
        System.out.print(moreDigit[nameOfDigit - 1][1] + " ");
      else if (dig1 >= 5)
        System.out.print(moreDigit[nameOfDigit - 1][2] + " ");
    }
  }
  private static void printThreeDigitsCount(int number, int degree)
  {
    int nameOfDigit, dig100, dig10, dig1;
    if (degree == 8 || degree == 5 || degree == 2)
    {
      nameOfDigit = degree / 4;
      number %= getCountByDegree(degree + 1);
      dig100 = (number / getCountByDegree(degree)) % 10;
      dig10 = (number / getCountByDegree(degree - 1)) % 10;
      dig1 = (number / getCountByDegree(degree - 2))  % 10;
      if (dig100 != 0)
        printDigit100(dig100);
      if (dig10 == 0 && dig1 == 0 && nameOfDigit != 0)
        System.out.print(moreDigit[nameOfDigit - 1][2] + " ");
    }
    else if (degree == 7 || degree == 4 || degree == 1)
    {
      nameOfDigit = degree / 3;
      number %= getCountByDegree(degree + 2);
      dig100 = 0;
      dig10 = (number / getCountByDegree(degree)) % 10;
      dig1 = (number / getCountByDegree(degree - 1))  % 10;
      if (dig10 != 0)
        printDigit10(dig10, dig1);
      if (dig1 == 0 && nameOfDigit != 0)
        System.out.print(moreDigit[nameOfDigit - 1][2] + " ");
    }
    else if (degree == 6 || degree == 3 || degree == 0)
    {
      nameOfDigit = degree / 3;
      number %= getCountByDegree(degree + 3);
      dig100 = (number / getCountByDegree(degree + 2)) % 10;
      dig10 = (number / getCountByDegree(degree + 1)) % 10;
      dig1 = (number / getCountByDegree(degree))  % 10;
      if (dig1 != 0 && dig10 != 1)
        printDigit1(dig1, nameOfDigit);
    }
  }
  private static void printCount(int number, int degree)
  {
    if (degree == 9)
      printMilliard(number, degree);
    else
      printThreeDigitsCount(number, degree);
  }
  public static void main(String[] args)
  {
    Scanner stream = new Scanner(System.in);
    int number = stream.nextInt();
    System.out.print("Исходное число: " + number + System.lineSeparator());
    int degree = getCountsOfDigits(number);
    degree--;
    System.out.print("Текстовое представление: ");
    if (number == 0)
      System.out.print("ноль");
    else
    {
      while (number > 0 && degree >= 0)
      {
        printCount(number, degree);
        degree--;
      }
    }
    System.out.print(System.lineSeparator());
  }
}
