package lab2;
import static java.lang.Math.*;

public class Task3
{
  public static class Point {
    private double x_;
    private double y_;
    Point(double x, double y) {
      this.x_ = x;
      this.y_ = y;
    }
    void setX(double x) {
      this.x_ = x;
    }
    void setY(double y) {
      this.y_ = y;
    }
    double getX(){
      return x_;
    }
    double getY(){
      return y_;
    }
    double getDistance(Point point){
      return Math.sqrt(Math.pow(point.x_ - x_, 2) + Math.pow(point.y_ - y_, 2));
    }
    void show(){
      System.out.print("(" + String.format("%.2f", x_) + ", " + String.format("%.2f", y_) + ")");
    }
  }

  public static class Triangle
  {
    private Point a_;
    private Point b_;
    private Point c_;
    Triangle(Point a, Point b, Point c){
      this.a_ = a;
      this.b_ = b;
      this.c_ = c;
    }
    void setA(Point a){
      this.a_ = a;
    }
    void setB(Point b){
      this.b_ = b;
    }
    void setC(Point c){
      this.c_ = c;
    }
    Point getA(){
      return a_;
    }
    Point getB(){
      return b_;
    }
    Point getC(){
      return c_;
    }
    double getAB(){
      return a_.getDistance(b_);
    }
    double getBC(){
      return b_.getDistance(c_);
    }
    double getAC(){
      return a_.getDistance(c_);
    }
    boolean isTriangle(){
      double a = getAB();
      double b = getBC();
      double c = getAC();
      if (a + b > c && a + c > b && b + c > a)
        return true;
      else
        return false;
    }
    double getPerimeter(){
      return getAB() + getBC() + getAC();
    }
    double getSquare(){
      double halfP = getPerimeter() / 2;
      return Math.sqrt(halfP * (halfP - getAB()) * (halfP - getBC()) * (halfP - getAC()));
    }
    Point getMedian(){
      double xM = (a_.x_ + b_.x_ + c_.x_) / 3;
      double yM = (a_.y_ + b_.y_ + c_.y_) / 3;
      Point medianPoint = new Point(xM, yM);
      return medianPoint;
    }
    void show(){
      System.out.print("Является ли треугольником:: ");
      if (isTriangle())
      {
        System.out.print("да" + System.lineSeparator());
        System.out.print("Координаты точек: " );
        a_.show();
        System.out.print(" , ");
        b_.show();
        System.out.print(" , ");
        c_.show();
        System.out.print(System.lineSeparator());
        System.out.print("Центр тяжести: ");
        getMedian().show();
        System.out.print(System.lineSeparator());
        System.out.print("Стороны: AB = " + String.format("%.2f", getAB()) + ", BC = " +
            String.format("%.2f", getBC()) + ", AC = " + String.format("%.2f", getAC()) + System.lineSeparator());
        System.out.print("Периметр: ");
        System.out.print(String.format("%.2f",getPerimeter()));
        System.out.print(System.lineSeparator());
        System.out.print("Площадь: ");
        System.out.print(String.format("%.2f",getSquare()));
      }
      else
        System.out.print("нет" + System.lineSeparator());
    }
  }

  public static void main(String[] args)
  {
    Point A = new Point(-4, -1);
    Point B = new Point(0, -3);
    Point C = new Point(2, 1);
    Triangle triangle = new Triangle(A, B, C);
    triangle.show();
  }
}
