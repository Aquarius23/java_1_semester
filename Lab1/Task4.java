package base;

public class MatrixOfTask4
{
    public static void main(String[] args)
    {
        int size = Integer.parseInt(args[0]);
        System.out.print("Size of the square matrix: " + Integer.toString(size) + System.lineSeparator());
        int matrix[][] = new int[size][];
        for (int i = 0; i < size; ++i)
            matrix[i] = new int[size];
        for (int i = 0; i  < size; ++i)
        {
            for (int j = 0; j < size; ++j)
                matrix[i][j] = 0;
        }
        for (int i = 0; i  < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                if (j <= i)
                    matrix[i][j] = 1;
            }
        }
        System.out.print("Final matrix: " + System.lineSeparator());
        for (int i = 0; i  < size; ++i)
        {
            for (int j = 0; j < size; ++j)
                System.out.print(Integer.toString(matrix[i][j]) + " ");
            System.out.print(System.lineSeparator());
        }
    }
}