package base;

public class ShellSorting {

    public static void main(String[] args)
    {
        int size = args.length;
        int[] array = new int[size];
        for (int i = 0; i < size; ++i)
            array[i] = Integer.parseInt(args[i]);
        System.out.print("Original array: ");
        for (int i = 0; i < size; ++i)
            System.out.print(Integer.toString(array[i]) + " ");
        System.out.print(System.lineSeparator());
        int interval = size / 2;
        while (interval >= 1)
        {
            for (int i = 0; i < size - interval; ++i)
            {
                int j = i;
                while (j >= 0 && array[j] > array[j + interval])
                {
                    int temp = array[j];
                    array[j] = array[j + interval];
                    array[j + interval] = temp;
                    j--;
                }
            }
            interval /= 2;
        }
        System.out.print("Sorted array: ");
        for (int i = 0; i < size; ++i)
            System.out.print(Integer.toString(array[i]) + " ");
        System.out.print(System.lineSeparator());
    }
}
