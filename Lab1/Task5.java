package base;
import static java.lang.Math.*;

public class MatrixOfTask5
{
    public static void main(String[] args)
    {
        int size = (int)sqrt(args.length);
        int matrix[][]= new int[size][];
        for (int i = 0; i < size; ++i)
            matrix[i] = new int[size];
        int k = 0;
        for (int i = 0; i < size; ++i)
        {
            for(int j = 0; j < size; ++j)
            {
                matrix[i][j] = Integer.parseInt(args[k]);
                ++k;
            }
        }
        System.out.print("Original matrix: " + System.lineSeparator());
        for (int i = 0; i < size; ++i)
        {
            for(int j = 0; j < size; ++j)
                System.out.print(Integer.toString(matrix[i][j]) + " ");
            System.out.print(System.lineSeparator());
        }
        System.out.print(System.lineSeparator());
        int[] maxElements = new int[size];
        for (int i = 0; i < size; ++i)
        {
            int maxElem = matrix[i][0];
            int index = 0;
            for (int j = 1; j < size; ++j)
                maxElem = (matrix[i][j] > maxElem) ? matrix[i][j] : maxElem;
            maxElements[i] = maxElem;
        }
        int minOfMax = maxElements[0];
        int index = 0;
        for (int i = 1; i < size; ++i)
        {
            index = (maxElements[i] < minOfMax) ? i : index;
            minOfMax = (maxElements[i] < minOfMax) ? maxElements[i] : minOfMax;
        }
        for (int i = 0; i < size; ++i)
        {
            if (i == index)
                matrix[i] = null;
        }
        System.out.print("Final matrix: " + System.lineSeparator());
        for (int i = 0; i < size; i++)
        {
            if(matrix[i] != null)
            {
                for (int j = 0; j < size; ++j)
                    System.out.print(Integer.toString(matrix[i][j]) + " ");
            }
            System.out.print(System.lineSeparator());
        }
        System.out.print(System.lineSeparator());
    }
}