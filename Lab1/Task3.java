package base;

public class UniqueElements
{
    public static void main(String[] args)
    {
        int size = args.length;
        int[] array = new int[size];
        for (int i = 0; i < size; ++i)
            array[i] = Integer.parseInt(args[i]);
        System.out.print("Original array: ");
        for (int i = 0; i < size; ++i)
            System.out.print(Integer.toString(array[i]) + " ");
        int countOfUniqueElem = 0;
        System.out.print(System.lineSeparator());
        for (int i = 0; i < size; ++i)
        {
            int j = 0;
            boolean isUnique = true;
            while (isUnique && j < size)
            {
                if (array[i] == array[j] && i != j)
                    isUnique = false;
                else
                    ++j;
            }
            if (isUnique && j == size)
                ++countOfUniqueElem;
        }
        System.out.print("Count of unique elements: " + Integer.toString(countOfUniqueElem) + System.lineSeparator());
    }
}