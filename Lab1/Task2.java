package base;
import java.util.Random;

public class RandomShuffle
{
    public static void main(String[] args)
    {
        int size = args.length;
        int[] arrayOriginal = new int[size];
        int[] arrayShuffled = new int[size];
        int[] indexArray = new int[size];
        for (int i = 0; i < size; ++i)
        {
            arrayOriginal[i] = Integer.parseInt(args[i]);
            indexArray[i] = i;
        }
        System.out.print("Original array: ");
        for (int i = 0; i < size; ++i)
            System.out.print(Integer.toString(arrayOriginal[i]) + " ");
        System.out.print(System.lineSeparator());
        Random rand = new Random();
        for (int i = size - 1; i >= 1; --i)
        {
            int randomIndexToShuffle = rand.nextInt(i);
            int temp = indexArray[randomIndexToShuffle];
            indexArray[randomIndexToShuffle] = indexArray[i];
            indexArray[i] = temp;
        }
        for (int i = 0; i < size; ++i)
            arrayShuffled[i] = arrayOriginal[indexArray[i]];
        System.out.print("Shuffled array: ");
        for (int i = 0; i < size; ++i)
            System.out.print(Integer.toString(arrayShuffled[i]) + " ");
        System.out.print(System.lineSeparator());
    }
}
