package lab4;
import java.util.Scanner;
import java.util.LinkedList;

public class Task1 {
  public static void main(String[] args)
  {
    Scanner stream = new Scanner(System.in);
    int count = stream.nextInt();
    LinkedList<Integer> list = new LinkedList<>();
    for (int  i = 0; i < count; ++i){
      list.add(i);
    }
    System.out.print("Исходный список:" + System.lineSeparator());
    System.out.print(list);
    System.out.print(System.lineSeparator());
    Integer N = stream.nextInt();
    shiftList(list, N%count);
    System.out.print("Список после циклического сдвига на " + N + ":" + System.lineSeparator());
    System.out.print(list);
  }

  static void shiftList(LinkedList<Integer> list, Integer shift)
  {
    int currentIndex, movedIndex, buffer;
    int count = greatestCommonDivisor(shift, list.size());
    for (int i = 0; i < count; ++i){
      buffer = list.get(i);
      currentIndex = i;

      if(shift > 0){
        while (true){
          movedIndex = currentIndex + shift;
          if (movedIndex >= list.size()){
            movedIndex -= list.size();
          }
          if (movedIndex == i){
            break;
          }
          list.set(currentIndex, list.get(movedIndex));
          currentIndex = movedIndex;
        }
      }
      else if (shift < 0){
        while (true){
          movedIndex = currentIndex + shift;
          if (movedIndex < 0){
            movedIndex += list.size();
          }
          if (movedIndex == i){
            break;
          }
          list.set(currentIndex, list.get(movedIndex));
          currentIndex = movedIndex;
        }
      }
      list.set(currentIndex, buffer);
    }
  }

  static int greatestCommonDivisor(int a, int b)
  {
    if (b == 0)
      return a;
    else
      return greatestCommonDivisor(b, a%b);
  }
}
