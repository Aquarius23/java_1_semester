package lab4;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task3 {
  public static <K,V> HashMap<V,K> replace(Map<K,V> map)
  {
    Map<V,K> fin = new HashMap<V,K>();
    for(Map.Entry<K,V> entry:map.entrySet()){
      fin.put(entry.getValue(), entry.getKey());
    }
    return (HashMap<V, K>) fin;
  }

  public static void main(String[] args)
  {
    Scanner stream = new Scanner(System.in);
    Map<Integer, String> map = new HashMap<>();

    map.put(1, "первый");
    map.put(3, "третий");
    map.put(6, "шестой");
    map.put(8, "восьмой");

    Map<String, Integer> reverseMap = replace(map);
    for(Map.Entry<String, Integer> entry:reverseMap.entrySet()){
      System.out.print(entry.getKey() + ": " + entry.getValue() + System.lineSeparator());
    }
  }
}
