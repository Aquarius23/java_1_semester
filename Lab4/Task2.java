package lab4;
import java.util.Stack;
import java.util.Scanner;

public class Task2 {
  public static int getDigit(int number)
  {
    int digit = 0;
    while (number > 0)
    {
      number /= 10;
      digit++;
    }
    return digit;
  }

  public static void main(String[] args) {
    Scanner stream = new Scanner(System.in);
    Stack<Integer> stack = new Stack<>();
    while (stream.hasNext())
    {
      int number = stream.nextInt();
      int radix = getDigit(number);
      for (int i = radix; i > 0; --i)
      {
        int digit = (int)((number % Math.pow(10, i)) / Math.pow(10, i - 1));
        stack.add(digit);
      }
      for (int i = stack.size() - 1; i >= 0; --i)
      {
        System.out.print(stack.get(i));
      }
      stack.clear();
      System.out.print(System.lineSeparator());
    }
  }
}
