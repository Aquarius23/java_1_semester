package lab3;

import java.io.CharConversionException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystemException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;

public class ExceptionTasks {
  public static void main(String[] args) {
    try {
      task5();
    } catch (NullPointerException error) {
      System.out.print(error.getClass());
    } catch (FileNotFoundException error) {
      System.out.print(error.getClass());
    }

    try {
      task6();
    } catch (Exception3 error) {
      System.out.print(error.getClass());
    } catch (Exception2 error) {
      System.out.print(error.getClass());
    } catch (Exception1 error) {
      System.out.print(error.getClass());
    }

    try {
      task7();
    } catch (FileSystemException error) {
      System.out.print(error.getClass());
    }

    task8();
  }

  private static void task1(){
    try {
      int a = 42 / 0;
    } catch (ArithmeticException error) {
      System.out.print(error.getClass());
    }
  }

  private static void task2(){
    try {
      String s = null;
      String m = s.toLowerCase();
    } catch (NullPointerException error) {
      System.out.print(error.getClass());
    }
  }

  private static void task3(){
    try {
      int[] m = new int[3];
      m[6] = 5;
    } catch (IndexOutOfBoundsException error){
      System.out.print(error.getClass());
    }
  }

  private static void task4(){
    try {
      int num = Integer.parseInt("XYZ");
      System.out.println(num);
    } catch (NumberFormatException error) {
      System.out.print(error.getClass());
    }
  }

  private static void task5() throws NullPointerException, ArithmeticException, FileNotFoundException, URISyntaxException {
    int i = (int) (Math.random() * 4);
    if (i == 0) {
      throw new NullPointerException();
    } else if (i == 1) {
      throw new ArithmeticException();
    } else if (i == 2) {
      throw new FileNotFoundException();
    } else if (i == 3) {
      throw new URISyntaxException("", "");
    }
  }

  private static void task6() throws Exception1 {
    int i = (int) (Math.random() * 3);
    if (i == 0) {
      throw new Exception1();
    } else if (i == 1) {
      throw new Exception2();
    } else if (i == 2) {
      throw new Exception3();
    }
  }

  private static void task7() throws FileSystemException{
    StatelessBean BEAN = new StatelessBean();
    try {
      StatelessBean.methodThrowExceptions();
    } catch (FileSystemException error) {
      StatelessBean.log(error);
      throw new FileSystemException("");
    } catch (CharConversionException error) {
      BEAN.log(error);
    } catch (IOException error) {
      BEAN.log(error);
    }
  }

  private static void task8(){
    ArrayList<Integer> array = new ArrayList<Integer>();
    Scanner input = new Scanner(System.in);
    try {
      while (input.hasNext()) {
        Integer temp = input.nextInt();
        array.add(temp);
      }
    } catch (InputMismatchException error) {
      for (Integer i = 0; i < array.size(); ++i) {
        System.out.print(array.get(i));
        System.out.print(System.lineSeparator());
      }
    }
  }

  static class Exception1 extends Exception{
  }

  static class Exception2 extends Exception1{
  }

  static class Exception3 extends Exception2{
  }

  public static class StatelessBean {
    public static void log(Exception exception) {
      System.out.println(exception.getMessage() + ", " + exception.getClass().getSimpleName());
    }

    public static void methodThrowExceptions() throws CharConversionException, FileSystemException, IOException {
      int i = (int) (Math.random() * 3);
      if (i == 0) {
        throw new CharConversionException();
      } else if (i == 1) {
        throw new FileSystemException("");
      } else if (i == 2) {
        throw new IOException();
      }
    }
  }
}